-- SPDX-FileCopyrightText: 2023 DS
--
-- SPDX-License-Identifier: CC0-1.0

ignore = {}

read_globals = {
	"minetest",
	"vector",
}

globals = {}
